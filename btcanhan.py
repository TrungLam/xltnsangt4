#!/usr/bin/env python
# coding: utf-8

# In[21]:


#1.Import the numpy package under the name np
import numpy as np


# In[22]:


#print out the numpy version and the configuration
Z = np.zeros(10)
print(Z)


# In[18]:


#3.How to find the memory size of any array
Z = np.zeros((10,10))
print("%d bytes" % (Z.size * Z.itemsize))


# In[27]:


#5.  Create a null vector of size 10 but the fifth value which is 1
Z = np.zeros(10)
Z[4] = 1
print(Z)


# In[28]:


#6..  Create a vector with values ranging from 10 to 49
Z = np.arange(10,50)
print(Z)


# In[29]:


#7.Reverse a vector (first element becomes last)
Z = np.arange(50)
Z = Z[::-1]
print(Z)


# In[30]:


# 8. Create a 3x3 matrix with values ranging from 0 to 8
Z = np.arange(9).reshape(3,3)
print(Z)


# In[31]:


#9. Find indices of non-zero elements from \[1,2,0,0,4,0\]
nz = np.nonzero([1,2,0,0,4,0])
print(nz)


# In[32]:


#10. Create a 5x6 zero matrix
Z = np.zeros( (5, 6) )
print(Z)


# In[37]:


#11.Create a 3x3 identity matrix
Z = np.eye(3)
print(Z)


# In[36]:


#12 Create a 3x3x3 array with random values
Z = np.random.random((3,3,3))
print(Z)


# In[38]:


#13.Create a 10x10 array with random values and find the minimum and maximum values
Z = np.random.random((10,10))
Zmin, Zmax = Z.min(), Z.max()
print(Zmin, Zmax)


# In[39]:


#14. Create a random vector of size 30 and find the mean value
Z = np.random.random(30)
A = Z.mean()
print(A)


# In[40]:


#15.Create a 2d array with 1 on the border and 0 inside
Z = np.ones((10,10))
Z[1:-1,1:-1] = 0
print(Z)


# In[41]:


16.# How to add a border (filled with 0's) around an existing array?
Z = np.ones((5,5))
Z = np.pad(Z, pad_width=1, mode='constant', constant_values=0)
print(Z)                        


# In[42]:


#17.What is the result of the following expression?
print(0 * np.nan)
print(np.nan == np.nan)
print(np.inf > np.nan)
print(np.nan - np.nan)
print(np.nan in set([np.nan]))
print(0.3 == 3 * 0.1)


# In[43]:


#18.Create a 5x5 matrix with values 1,2,3,4 just below the diagonal
Z = np.diag(1+np.arange(4),k=-1)
print(Z)


# In[44]:


#19.Create a 8x8 matrix and fill it with a checkerboard pattern
Z = np.zeros((8,8),dtype=int)
Z[1::2,::2] = 1
Z[::2,1::2] = 1
print(Z)


# In[45]:


#20.Consider a (6,7,8) shape array, what is the index (x,y,z) of the 100th element?
print(np.unravel_index(99,(6,7,8)))


# In[46]:


#21. Create a checkerboard 8x8 matrix using the tile function
Z = np.tile( np.array([[0,1],[1,0]]), (4,4))
print(Z)


# In[47]:


#22. Normalize a 5x5 random matrix
Z = np.random.random((5,5))
Z = (Z - np.mean (Z)) / (np.std (Z))
print(Z)


# In[50]:


#24 Multiply a 5x3 matrix by a 3x2 matrix (real matrix product)
Z = np.dot(np.ones((5,3)), np.ones((3,2)))
print(Z)
Z = np.dot(np.ones((5,3)), np.ones((3,2)))


# In[51]:


#25.Given a 1D array, negate all elements which are between 3 and 8, in place
Z = np.arange(11)
Z[(3 < Z) & (Z <= 8)] *= -1
print(Z)


# In[ ]:




