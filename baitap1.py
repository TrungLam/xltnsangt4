# bai_1
s = 'Hi John, welcome to python programming for beginner!'
# a. kiem tra xem trong chuoi s co tu 'python' ko?
a = 'python'
b = a in s
print(b)
# b. trich xuat tu 'John' trong chuoi s luu vao bien s1
s1 = s[3:7]
print(s1)
# c. dem co bao nhieu ky tu 'o' trong s
print(s.count('o'))
# d. dem co bao nhieu tu trong s. ham split() o day la ham tach chuoi bo di cac dau space
print(len(s.split(' ')))

print("-------------------------------------------------")
# Bai_2
I = [23,4.3,4.2,31,'python',1,5.3,9,1.7]
print(I)
# a. Xoa tu python khoi list
I.remove('python')
print(I)
# b. sap xep tang dan
I.sort()
print(I)
# sap xep giam dan
I.reverse()
print(I)
#c. check xem 4.2 co trong I hay khong
b = 4.2 in I
print(b)

print("---------------------------------------------------")
#Bai 4
a = (1, 'python', [2,3], [4,5])
x, y,(z,d),(c,e) = a
t=(x , y , z, d, c, e)
print(t)
print("Phần tử cuối cùng của t: ", t[-1])
str = [2,3]
str.append(t)
print(str)
#chuyen tuple t ve list
str = list(t)
print(str)

print("------------------------------------------------------")
#Bai 5
dic1 = {1:10, 2:20}
dic2 = {3:30, 4:40}
dic3 = {5:50, 6:60}

dic1.update(dic2)
dic1.update(dic3)
print(dic1)

print("-----------------------------------------------------")
#Câu 8
import numpy
import random
import re
import string
lst = (numpy.random.choice(50, 1000, replace=True))
print(lst)
x = int(input('nhap vao so x = '))
def count(lst, x):
    dem = 0
    for so in lst:
        if (so == x):
            dem = dem + 1
    return dem
print('số {} xuất hiện {} lần'.format(x, count(lst, x)))

print("------------------------------------------------------")
#cau 9
import matplotlib.pyplot as plt
n=50
x = numpy.arange(n)
y = 0.2*numpy.cos(x)
plt.plot(x,y,'r-',linewidth=2)
plt.show()


print("-----------------------------------------------------")
#Bai 10
import numpy as np
A = np.array([[1,2,3], [4,5,6]])
B = np.array([[2,3], [4,5], [6,7]])
A = A.dot(B)
print('Ma trận')
print(A)

print("-----------------------------------------------------")
#Câu 12:
sanpham =  ['thịt', 'cá', 'trứng', 'sữa']
tensp = ['tên1', 'tên2', 'tên3', 'tên4']
giasp = [80,50,40,70]
print('Các loại sp gồm : ',sanpham)
print('Tên sp gồm : ',tensp)
print('Giá sp gồm : ',giasp)
print('----------BẢNG BÁO GIÁ----------')
for z in range (0, len(sanpham)):
    if (z==0):
        print('Loại sp: ',sanpham[0])
        print('Tên sp: ',tensp[0])
        print('Giá sp : ',giasp[0])
    elif (z==1):
        print('Loại sp: ',sanpham[1])
        print('Tên sp: ',tensp[1])
        print('Giá sp : ',giasp[1])
    elif (z==2):
        print('Loại sp: ',sanpham[2])
        print('Tên sp: ',tensp[2])
        print('Giá sp : ',giasp[2])
    else :
        print('Loại sp: ',sanpham[3])
        print('Tên sp: ',tensp[3])
        print('Giá sp : ',giasp[3])

print('Tổng giá = ',sum(giasp))
sapxep = sorted(giasp,reverse = True)
print(giasp.sort)
Dic = {
       80 : "thịt " 
            "tên1 "
            "80",
       70 : "sữa "
            "tên4 "
            "70",
       50 : "cá "
            "tên2 "
            "50",
       40 : "trứng "
            "tên3 "
            "40"
       }
print('Ba sp hàng đầu gồm : ')
for k in range (0,(len(sapxep))-1):
    if (k==0):
        print(Dic[80])
    elif (k==1):
        print(Dic[70])
    else :
        print(Dic[50])