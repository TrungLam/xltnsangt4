#!/usr/bin/env python
# coding: utf-8

# In[1]:


#câu1
l1=[3,4,'hello',(4.6,5.6),[3,7,2,2,[0.3,5],'text'],6,3,3.5]
def tubb(x):
    t=0;
    for i in x:
        tp=str(type(i))
        if 'float'in tp or 'int'in tp:
            t+=i
    return t
def lis(x):
    t=0
    for i in x:
        tp = str(type(i))
        if 'float'in tp or 'int'in tp:
            t+=i
        if 'tuple' in tp:
            t +=tubb(i)
        if 'list' in tp:
            t += lis(i)
    return t
print('a. ket qua: ',lis(l1))
print('b. nhap list bat ky:')
l2=input()
l2=list(l2)
print('ket qua:',lis(l2))


# In[9]:


#cau 2
l1=[2,4,6,4,9,4,7]
l2=[5,3,5,6,9,7,5]
def max_of_pair(l1,l2):
    print('Without key argument, the maximum list:', max(l1, l2))
    return max_of_pair
print(max(l1,l2))


# In[15]:


#cau3
f= open(r"D:\university's subject\python leaning\week3_1.txt","r")
def sumword(x):
    str=x.readlines()
    print(str)
    lismap=list(map(len,str))
    dem=0
    for i in lismap: 
        dem+=i
    return dem-len(str)+1
print(sumword(f))


# In[ ]:




