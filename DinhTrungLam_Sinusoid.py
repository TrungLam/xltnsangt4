#!/usr/bin/env python
# coding: utf-8

# In[28]:


# câu 1.
get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as plt
A = .8 #biên độ của phuờng trình sóng
f = 5 #tần số của tín hiệu là 5
t = np.arange(0,1,.01)#các điểm trên trục x để vẽ đồ thị
phi = np.pi/4 # Độ trễ pha là pi/4
x = A*np.cos(2*np.pi*f*t + phi)# tính giá trị của mỗi tần số lấy mẫu
plt.plot(t,x) #vẽ đồ thị 
plt.axis([0,1,-1,1]) #xác định tọa độ x, y
plt.xlabel('time in seconds')# dán nhãn trục x 
plt.ylabel('amplitude')# dán nhãn cho trục y
plt.show()# hiểm thị sóng


# In[29]:


#câu 2
get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as plt
A = .65  #biên độ của đồ thị
fs = 100 # tỷ lệ mẫu
samples = 100 # biến có 100 phần tử
f = 5 #tần số dao động của tín hiệu là 5
phi = 0 # Độ trễ pha là 0
n = np.arange(samples) #trả về khoảng cách 100
T=1.0/fs # sử dụng 1.0 không sử dụng 1
y= A*np.cos(2*np.pi*f*n*T + phi) # tính giá trị của mỗi tần số lấy mẫu
plt.plot(y) # vẽ đồ thị
plt.axis([0,100,-1,1])  #tạo ta khung để giới hạn theo trục x và y
plt.xlabel('sample index') # dán nhãn  cho trục x của đồ thị
plt.ylabel('amplitude') # dán nhãn cho trục y của đồ thị
plt.show() # hiển thị sóng


# In[30]:


#câu 3
get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as plt
A = .8 #biên độ của đồ thị
N = 100 # số lượng mẫu là 100
f = 5 # tần số của tín hiệu là 5
phi = 0 # Độ trễ pha là 0
n = np.arange(N)
y = A*np.cos(2*np.pi*f*n/N + phi) # tính giá trị của mỗi tần số lấy mẫu
plt.plot(y) # vẽ đồ thị
plt.axis([0,100,-1,1])#tạo ta khung để giới hạn theo trục x và y
plt.xlabel('sample index') # dán nhãn  cho trục x của đồ thị
plt.ylabel('amplitude') # dán nhãn cho trục y của đồ thị
plt.show() # hiển thị sóng


# In[37]:


#câu 4
get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as plt
f = 3 # tần số của tín hiệu là 3
t = np.arange(0,1,.01)# các điểm trên trục x để vẽ đồ thị
phi = 0 # Độ trễ pha là 0
x = np.exp(1j*(2*np.pi*f*t + phi))# tính giá trị của mỗi tần số lấy mẫu
xim = np.imag(x) # lấy phần ảo 
plt.figure(1) # tạo cửa sổ đồ họa mới
plt.plot(t,np.real(x)) # vẽ đồ thị trả về phần ảo
plt.plot(t,xim) # vẽ đồ thị trả về phần ảo
plt.axis([0,1,-1.1,1.1])# xác định tọa độ x, y
plt.xlabel('time in seconds')# dán nhãn  cho trục x của đồ thị
plt.ylabel('amplitude') # dán nhãn cho trục y của đồ thị
plt.show()# hiển thị sóng


# In[33]:


#câu 5
get_ipython().magic(u'matplotlib inline')
import numpy as np
import matplotlib.pyplot as plt
f = 3  # tần số của tín hiệu là 3
N = 100  # số lượng mẫu là 100
fs = 100 # tỷ lệ mẫu
n = np.arange(N)
T = 1.0/fs # sử dụng 1.0 không sử dụng 1
t = N*T
phi = 0 # Độ trễ pha là 0
x = np.exp(1j*(2*np.pi*f*n*T + phi))# hàm mũ
xim = np.imag(x)# lấy phần ảo 
plt.figure(1) # tạo cửa sổ đồ họa mới
plt.plot(n*T,np.real(x)) # vẽ đồ thị trả về phần ảo
plt.plot(n*T,xim) # vẽ đồ thị trả về phần ảo
plt.axis([0,t,-1.1,1.1])# xác định tọa độ x, y
plt.xlabel('t(seconds)')# dán nhãn  cho trục x của đồ thị
plt.ylabel('amplitude') # dán nhãn cho trục y của đồ thị
plt.show()# hiển thị sóng


# In[34]:


#câu 6
f = 3 # tần số của tín hiệu là 3
N = 64  # số lượng mẫu là 64
n = np.arange(64)# trả về 64 phần tử
phi = 0 # Độ trễ pha là 0
x = np.exp(1j*(2*np.pi*f*n/N + phi))# hàm mũ
xim = np.imag(x) # lấy phần ảo 
plt.figure(1) # tạo cửa sổ đồ họa mới
plt.plot(n,np.real(x)) # vẽ đồ thị trả về phần ảo
plt.plot(n,xim) # vẽ đồ thị trả về phần ảo
plt.axis([0,samples,-1.1,1.1]) # xác định tọa độ x, y
plt.xlabel('sample index')# dán nhãn  cho trục x của đồ thị
plt.ylabel('amplitude') # dán nhãn cho trục y của đồ thị
plt.show()# hiển thị sóng


# In[35]:


# câu 7
N = 44100 # số lượng mẫu là 44100
f = 440 # tần số của tín hiệu là 440
fs = 44100 # tỷ lệ mẫu
phi = 0 # Độ trễ pha là 0
n = np.arange(N)# trả về mảng có N phần tử  tương ứng
x = A*np.cos(2*np.pi*f*n/N + phi)
from scipy.io.wavfile import write
write('sine440_1sec.wav',44100,x)# ghi vào giá trị 


# In[ ]:




